CFLAGS=-g

.PHONY: all
all: testIO testRead

testIO: testIO.c
	gcc $(CFLAGS) -o testIO testIO.c

testRead: testRead.c
	gcc $(CFLAGS) -o testRead testRead.c

.PHONY: clean
clean:
	rm testIO testRead
