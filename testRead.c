#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
	int fd1,fd2;
	fd1 = open("foo.txt", O_RDWR|O_APPEND, 0);

	write(fd1,"hola\n",5);
	
	close(fd1);
	
	fd2 = open("bar.txt",O_RDWR,0);
	//char buffer[] = {'0','1','2','3','4','5','6','7','8','9',0};
	char buffer[100];
	int s = read(fd2,buffer,10);

	printf("s=%d\n",s);
	printf("%s",buffer);
	
	close(fd2);
}
